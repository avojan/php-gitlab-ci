<?php

namespace Avojan;

require_once dirname(__FILE__) . "/../vendor/autoload.php";

use Avojan\PrivateLib\Hello;

echo "Hello world" . PHP_EOL;

$hello = new Hello();

echo $hello->say("Avetis") . PHP_EOL;
